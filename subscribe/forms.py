from django import forms
from . import models

class SubscriberForm(forms.Form):
    name = forms.CharField(label='Nama', widget=forms.TextInput(attrs={
            'class': 'form-control',
            'required': True,
            'onInput': 'validate()',
            'placeholder': 'Nama',
    }), max_length=50)

    email = forms.EmailField(label='Email', widget=forms.EmailInput(attrs={
            'class': 'form-control',
            'required': True,
            'onInput': 'validate()',
            'placeholder': 'Email',
    }), max_length=50)

    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={
            'class': 'form-control',
            'required': True,
            'onInput': 'validate()',
            'placeholder': 'Password',
    }), max_length=50)

    class Meta:
        model = models.Subscriber
