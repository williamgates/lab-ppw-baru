function validate() {
    event.preventDefault(true)
    var fields = document.forms["subscribe-form"].getElementsByTagName("input")
    var csrf_token = fields[0].value
    var name = fields[1].value
    var email = fields[2].value
    var password = fields[3].value

    $.ajax({
        url: '/subscribe/form-check/',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify({
            name: name,
            email: email,
            password: password
        }),
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', csrf_token)
        },
        success: function(response) {
            var button = document.getElementById("submit")
            var report = document.getElementById("report")
            report.classList.remove("error-report", "success-report")
            
            if (response['is_valid']) {
                if (response['is_exist']) {
                    button.disabled = true
                    report.classList.add("error-report")
                    report.innerText = "Email telah digunakan"
                } else { 
                    button.disabled = false
                    report.classList.add("success-report")
                    report.innerText = "Tidak ada kesalahan"
                }
            } else {
                button.disabled = true
                report.classList.add("error-report")
                report.innerText = "Tidak valid"
            }
        }
    })
}

$("#submit").click(function () {
    event.preventDefault(true)
    var form = document.forms["subscribe-form"]
    var fields = form.getElementsByTagName("input")
    var csrf_token = fields[0].value
    var name = fields[1].value
    var email = fields[2].value
    var password = fields[3].value

    $.ajax({
        url: '/subscribe/create-subscriber/',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify({
            name: name,
            email: email,
            password: password,
        }),
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', csrf_token)
        },
        complete: function(response) {
            var button = document.getElementById("submit")
            var report = document.getElementById("report")
            report.classList.remove("error-report", "success-report")

            button.disabled = true
            form.reset()
            report.classList.add("success-report")
            report.innerHTML = "Berhasil!"
        }
    })
});
