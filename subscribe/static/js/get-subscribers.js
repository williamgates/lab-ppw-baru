$(document).ready(function () {
    $.ajax({
        method: "GET",
        url: "get-subscribers/",
        success: function(response) {
            $("#subscribers-list").html('')
            var all_subscribers = response.data

            for (var subscriber_id in all_subscribers) {
                var button = "<button" +
                             " class=\"btn btn-light btn-sm\"" +
                             " onclick=\"unsubscribe('" + subscriber_id + "', '" + all_subscribers[subscriber_id][1] + "')\"" +
                             " type=\"submit\"" +
                             " value=\"Unsubscribe\"" +
                             ">" +
                             "Unsubscribe" +
                             "</button>"
                var row = "<span" + 
                          " id=" + subscriber_id +
                          ">" +
                          (parseInt(subscriber_id) + 1) + ". " +
                          all_subscribers[subscriber_id][0] +
                          " - " +
                          all_subscribers[subscriber_id][1] +
                          "&nbsp;&nbsp;&nbsp;" + button +
                          "</span>" +
                          "<br /><br />"
                $("#subscribers-list").append(row)
            }
        }
    })
});

function unsubscribe(id, email) {
    $.ajax({
        method: "POST",
        url: "unsubscribe/",
        data: {email: email,},
        success: function(response) {
            $("#" + id).remove()
        }
    })
}
