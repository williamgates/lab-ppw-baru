from django.urls import path
from . import views

app_name = 'subscribe'
urlpatterns = [
    path('', views.subscribe, name='subscribe'),
    path('create-subscriber/', views.create_subscriber, name='create-subscriber'),
    path('form-check/', views.form_check, name='form-check'),
    path('subscribers-list/', views.subscribers_list, name='subscribers-list'),
    path('subscribers-list/get-subscribers/', views.get_subscriber, name='get-subscribers'),
    path('subscribers-list/unsubscribe/', views.unsubscribe, name='unsubscribe'),
]
