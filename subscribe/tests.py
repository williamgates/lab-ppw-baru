import json
from django.test import Client, TestCase
from django.urls import resolve
from .models import Subscriber
from .views import subscribe, form_check, create_subscriber, get_subscriber, subscribers_list, unsubscribe

class SubscriberModelTest(TestCase):
    def test_string_representation(self):
        subscriber = Subscriber(name='Nama', email='email@example.com', password='password')
        self.assertEqual(str(subscriber), subscriber.email)

class UrlTests(TestCase):
    def test_subscribe_url(self):
        response = self.client.get('/subscribe/')
        self.assertEqual(response.status_code, 200)
    
    def test_create_subscriber_url(self):
        response = self.client.get('/subscribe/create-subscriber/')
        self.assertEqual(response.status_code, 403)

    def test_form_check_url(self):
        response = self.client.get('/subscribe/form-check/')
        self.assertEqual(response.status_code, 403)
    
    def test_subscriber_list(self):
        response = self.client.get('/subscribe/subscribers-list/')
        self.assertEqual(response.status_code, 200)

    def test_get_subscribers(self):
        response = self.client.get('/subscribe/subscribers-list/get-subscribers/')
        self.assertEqual(response.status_code, 200)

    def test_unsubscribe(self):
        response = self.client.get('/subscribe/subscribers-list/unsubscribe/')
        self.assertEqual(response.status_code, 403)

    def test_invalid_url(self):
        response = self.client.get('/invalid-url/')
        self.assertEqual(response.status_code, 404)

class SubscriberTests(TestCase):
    def test_get_subscribers(self):
        Subscriber(name='Nama', email='email@example.com', password='password').save()
        self.client.get('/subscribe/subscribers-list/get-subscribers/')
