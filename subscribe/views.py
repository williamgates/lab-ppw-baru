import json
from django.http import JsonResponse, HttpResponse, HttpResponseForbidden
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .forms import SubscriberForm
from .models import Subscriber

def subscribe(request):
    form = SubscriberForm()
    response = {
        'form': form
    }
    return render(request, 'subscribe/subscribe.html', response)

def form_check(request):
    if (request.method == 'POST' and request.is_ajax()):
        json_data = json.loads(request.body, encoding='UTF-8')
        is_exist = Subscriber.objects.filter(email=json_data['email']).exists()
        form = SubscriberForm({
            'name': json_data['name'],
            'email': json_data['email'],
            'password': json_data['password'],
        })
        return JsonResponse({
            'is_valid': form.is_valid(),
            'is_exist': is_exist,
        })
    else:
        return HttpResponseForbidden()

def create_subscriber(request):
    if (request.method == 'POST' and request.is_ajax()):
        json_data = json.loads(request.body, encoding='UTF-8')        
        Subscriber.objects.create(
            name = json_data['name'],
            email = json_data['email'],
            password = json_data['password']
        )
        return HttpResponse('Berhasil!')
    else:
        return HttpResponseForbidden()

def get_subscriber(request):
    all_subscribers = {}
    subscribers = Subscriber.objects.all()
    
    id = 0
    for subscriber in subscribers:
        all_subscribers[id] = [subscriber.name, subscriber.email]
        id += 1

    return JsonResponse({'data': all_subscribers})

def subscribers_list(request):
    return render(request, 'subscribe/subscribers-list.html', {})

@csrf_exempt
def unsubscribe(request):
    if (request.method == 'POST' and request.is_ajax()):
        subscriber = Subscriber.objects.filter(email=request.POST['email']).first()
        subscriber.delete()
        return HttpResponse('Dihapus!')
    else:
        return HttpResponseForbidden()

