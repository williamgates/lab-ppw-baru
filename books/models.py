from django.db import models

class Favorite(models.Model):
    book_id = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.book_id
