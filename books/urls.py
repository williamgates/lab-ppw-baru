from django.urls import path
from . import views

app_name = 'books'
urlpatterns = [
    path('home/', views.home, name='home'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('', views.books, name='books'),
    path('get-books/', views.get_books, name='get-books'),
    path('toggle/', views.toggle, name='toggle'),
]
