from django.test import Client, TestCase
from django.urls import resolve
from .models import Favorite
from .views import books, get_books, toggle

class FavoriteModelTest(TestCase):
    def test_string_representation(self):
        favorite = Favorite(book_id='xxxxxxxxxx')
        self.assertEqual(str(favorite), favorite.book_id)

class UrlTests(TestCase):
    def test_home_url(self):
        response = self.client.get('/books/home/')
        self.assertEqual(response.status_code, 302)

    def test_login_url(self):
        response = self.client.get('/books/login/')
        self.assertEqual(response.status_code, 200)

    def test_logout_url(self):
        response = self.client.get('/books/logout/')
        self.assertEqual(response.status_code, 302)
    
    def test_books_url(self):
        response = self.client.get('/books/')
        self.assertEqual(response.status_code, 302)

    def test_toggle_url(self):
        response = self.client.get('/books/toggle/')
        self.assertEqual(response.status_code, 200)

    def test_invalid_url(self):
        response = self.client.get('/invalid-url/')
        self.assertEqual(response.status_code, 404)
