$(".search-button").click(function () {
    $.ajax({
        method: "GET",
        url: "get-books/",
        data: {keyword: $(this).attr('value'),},
        success: function(response) {
            $("tbody").html('');
            var all_books = response.data;

            for (var book_id in all_books) {
                var img = "<img" +
                          " id=\"" + book_id + "\"" +
                          " src=\"" + (all_books[book_id].is_favorite ? "/static/img/star1.png\"" : "/static/img/star2.png\"") +
                          " width=\"30\"" +
                          " onclick=\"toggle('" + book_id + "')\"" +
                          " style=\"cursor: pointer\"" +
                          " />"
                var row = "<tr>" +
                          "<td>" + all_books[book_id].title + "</td>" +
                          "<td>" + all_books[book_id].authors + "</td>" +
                          "<td>" + all_books[book_id].publisher + "</td>" +
                          "<td>" + all_books[book_id].published_date + "</td>" +
                          "<td>" + img + "</td>" +
                          "</tr>";
                $("tbody").append(row);
            }
        }
    })
});

function toggle(book_id) {
    $.ajax({
        method: "POST",
        url: "toggle/",
        data: {book_id: book_id,},
        success: function(response) {
            if (response.is_favorite) {
                $("#" + book_id).attr("src", "/static/img/star1.png");
            }
            else {
                $("#" + book_id).attr("src", "/static/img/star2.png");
            }
            $("#favorite_count").html(response.count);
        }
    })
}
