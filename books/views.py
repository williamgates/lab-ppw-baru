import requests
from google.auth.transport import requests as google_requests
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate
from django.http import JsonResponse
from django.shortcuts import render, HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from .models import Favorite
from google.oauth2 import id_token
from django.urls import reverse

def home(request):
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('books:login'))

    user_id = request.session['user_id']
    email = request.session['email']
    name = request.session['name']
    picture = request.session['picture']
    return render(request, 'books/home.html', {'user': {'user_id': user_id, 'email': email, 'name': name, 'picture': picture}})

def login(request):
    if request.method == "POST":
        try:
            token = request.POST['id_token']
            id_info = id_token.verify_oauth2_token(token, google_requests.Request(), "958782081053-pg3bg2ul602qnvcgifunl88omhspbh4c.apps.googleusercontent.com")
            if id_info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')
            user_id = id_info['sub']
            email = id_info['email']
            name = id_info['name']
            picture = id_info['picture']
            request.session['user_id'] = user_id
            request.session['email'] = email
            request.session['name'] = name
            request.session['book'] = []
            request.session['picture'] = picture
            return JsonResponse({"status": "0", 'url': reverse("books:home")})
        except ValueError:
            return JsonResponse({"status": "1"})
    return render(request, 'books/login.html')

def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('books:login'))

def books(request):
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('books:login'))

    name = request.session['name']
    count = len(request.session['book'])
    return render(request, 'books/books.html', {'name': name, 'count': count})

def get_books(request):
    all_books = {}
    keyword = request.GET['keyword']
    books_json = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + keyword).json()
    books_list=  books_json['items']

    for book in books_list:
        title = book['volumeInfo'].get('title')
        authors = book['volumeInfo'].get('authors')
        publisher = book['volumeInfo'].get('publisher')
        published_date = book['volumeInfo'].get('publishedDate')
        is_favorite = False
        if (book['id'] in request.session['book']):
            is_favorite = True
        all_books[book['id']] = {'title': title,
                                 'authors': authors,
                                 'publisher': publisher,
                                 'published_date': published_date,
                                 'is_favorite': is_favorite}

    return JsonResponse({'data': all_books})

@csrf_exempt
def toggle(request):
    count = 0
    is_favorite = False

    if (request.method == 'POST'):
        books = request.session['book']
        book_id = request.POST['book_id']

        if book_id in request.session['book']:
            books.remove(book_id)
            is_favorite = False
        else:
            books.append(book_id)
            is_favorite = True

        count = len(books)
        request.session['book'] = books
        print(books)

    return JsonResponse({'is_favorite': is_favorite, 'count': count})
