from django import forms
from .models import Status

class StatusForm(forms.ModelForm):
    text = forms.CharField(widget=forms.TextInput(attrs={
        'class'         : 'form-control',
        'required'      : True,
        'placeholder'   : 'Kabar saya...'
    }), max_length=300)

    class Meta:
        model = Status
        fields = ('text',)
