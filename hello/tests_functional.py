# from django.test import Client, LiveServerTestCase, TestCase
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# from selenium.webdriver.support.color import Color

# class HomepageTest(LiveServerTestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')        
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         self.page_url = 'https://lab-ppw-baru.herokuapp.com'
#         super(HomepageTest, self).setUp()

#     def tearDown(self):
#         self.browser.quit()
#         super(HomepageTest, self).tearDown()

#     def test_content_positioning(self):
#         self.browser.get(self.page_url)
#         self.browser.set_window_size(1280, 720)

#         navbar = self.browser.find_element_by_tag_name('nav')
#         self.assertEqual(navbar.location['x'], 0)
#         self.assertEqual(navbar.location['y'], 0)
#         self.assertEqual(navbar.size['width'], 1280)

#         title = self.browser.find_element_by_tag_name('h1')
#         self.assertAlmostEqual(title.location['x'] + title.size['width'] / 2, 640, delta=10)

#         status_input_box = self.browser.find_element_by_id('id_text')
#         self.assertAlmostEqual(status_input_box.location['x'] + status_input_box.size['width'] / 2, 640, delta=10)

#     def test_css_property(self):
#         self.browser.get(self.page_url)
#         background_color = self.browser.find_element_by_tag_name('body').value_of_css_property('background-color')
#         background_color = Color.from_string(background_color).hex
#         self.assertEquals(background_color, '#000000')

#         status_input_box_color = self.browser.find_element_by_id('id_text').value_of_css_property('color')
#         status_input_box_color = Color.from_string(status_input_box_color).hex
#         self.assertEquals(status_input_box_color, '#495057')

#     def test_submit_status(self):
#         self.browser.get(self.page_url)
#         status_input_box = self.browser.find_element_by_id('id_text')
#         submit_button = self.browser.find_element_by_id('submit')
#         status_input_box.send_keys('Coba Coba')
#         submit_button.send_keys(Keys.RETURN)
#         self.assertIn('Coba Coba', self.browser.page_source)
