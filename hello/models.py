from django.db import models
from django.utils import timezone

class Status(models.Model):
    text = models.CharField(max_length=300)
    time = models.TimeField(default=timezone.localtime(timezone.now()))

    class Meta:
        verbose_name_plural = "statuses"

    def __str__(self):
        return self.text
