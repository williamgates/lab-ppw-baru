# Generated by Django 2.1.1 on 2018-11-22 02:32

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('hello', '0006_auto_20181122_0915'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='time',
            field=models.TimeField(default=datetime.datetime(2018, 11, 22, 2, 32, 14, 671385, tzinfo=utc)),
        ),
    ]
