from django.urls import path
from . import views

app_name = 'hello'
urlpatterns = [
    path('', views.index, name='index'),
    path('add-status/', views.add_status, name='add-status'),
    path('profile/', views.profile, name='profile'),
]
