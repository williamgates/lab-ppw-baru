from django import forms
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from .forms import StatusForm
from .models import Status

def index(request):
    status_form = StatusForm()
    statuses = Status.objects.all()
    context = {'status_form' : status_form, 'statuses': statuses}
    return render(request, 'hello/index.html', context)

def add_status(request):
    status_form = StatusForm(request.POST)
    if request.method == 'POST' and status_form.is_valid():
        Status.objects.create(text=status_form.cleaned_data.get('text'))
    return HttpResponseRedirect(reverse('hello:index'))

def profile(request):
    return render(request, 'hello/profile.html', {})
