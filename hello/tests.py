from datetime import date, datetime, timedelta
from django.test import Client, LiveServerTestCase, TestCase
from django.urls import resolve
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .forms import StatusForm
from .models import Status
from .views import add_status, index, profile

class StatusModelTest(TestCase):
    def test_string_representation(self):
        status = Status(text="Status saya")
        self.assertEqual(str(status), status.text)

    def test_verbose_name_plural(self):
        self.assertEqual(str(Status._meta.verbose_name_plural), "statuses")

class UrlTests(TestCase):
    def test_homepage_url(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_add_status_url(self):
        response = self.client.get('/add-status/')
        self.assertEqual(response.status_code, 302)

    def test_profile_url(self):
        response = self.client.get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_invalid_url(self):
        response = self.client.get('/invalid-url/')
        self.assertEqual(response.status_code, 404)

class HomepageTests(TestCase):
    def test_homepage_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_homepage_using_index_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'hello/index.html')

    def test_homepage_header(self):
        response = self.client.get('/')
        self.assertContains(response, 'Hello apa kabar?')

    def test_no_status(self):
        response = self.client.get('/')
        self.assertContains(response, 'Kosong')
        self.assertEqual(Status.objects.all().count(), 0)

    def test_one_status(self):
        Status.objects.create(text='Status saya')
        response = self.client.get('/')
        self.assertContains(response, 'Status saya')
        self.assertEqual(Status.objects.all().count(), 1)

    def test_two_status(self):
        Status.objects.create(text='Status pertama saya')
        Status.objects.create(text='Status kedua saya')
        response = self.client.get('/')
        self.assertContains(response, 'Status pertama saya')
        self.assertContains(response, 'Status kedua saya')
        self.assertEqual(Status.objects.all().count(), 2)

class AddStatusTests(TestCase):
    def test_add_status_using_add_status_func(self):
        found = resolve('/add-status/')
        self.assertEqual(found.func, add_status)

    def test_post_status(self):
        self.client.post('/add-status/', {
            'text': 'Status saya'
        })
        self.assertEqual(Status.objects.first().text, 'Status saya')
        time_threshold = datetime.combine(date.today(), Status.objects.first().time) - datetime.combine(date.today(), datetime.now().time())
        self.assertLessEqual(time_threshold, timedelta(seconds=60))
        self.assertEqual(Status.objects.count(), 1)

    def test_redirect_after_post_status(self):
        response = self.client.post('/add-status/', {
            'text': 'Status saya'
        })
        self.assertEqual(response.status_code, 302)

class ProfileTests(TestCase):
    def test_profile_using_profile_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)
    
    def test_profile_using_profile_template(self):
        response = self.client.get('/profile/')
        self.assertTemplateUsed(response, 'hello/profile.html')

    def test_profile_header(self):
        response = self.client.get('/profile/')
        self.assertContains(response, 'Profil Saya')

class StatusFormTest(TestCase):
    def test_blank_data(self):
        form = StatusForm({})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            'text': ['This field is required.'],
        })

    def test_valid_data(self):
        form = StatusForm({
            'text': "Status saya",
        })
        self.assertTrue(form.is_valid())
        status = form.save()
        self.assertEqual(status.text, "Status saya")

    def test_invalid_data(self):
        form = StatusForm({
            'text': 301 * '.'
        })
        self.assertFalse(form.is_valid())

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_submit_status(self):
        self.browser.get(self.live_server_url)
        status_input_box = self.browser.find_element_by_id('id_text')
        submit_button = self.browser.find_element_by_id('submit')
        status_input_box.send_keys('Coba Coba')
        submit_button.send_keys(Keys.RETURN)
        self.assertIn('Coba Coba', self.browser.page_source)
